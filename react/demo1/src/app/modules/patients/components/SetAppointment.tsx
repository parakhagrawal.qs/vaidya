import { Field, Formik } from 'formik'
import { FC, useState } from 'react'
import { Modal } from 'react-bootstrap'
import {KTSVG} from "../../../../_metronic/helpers/components/KTSVG";

type Props = {
    showModal: boolean
    closeModal: Function
}

const SetAppointment: FC<Props> = ({ showModal, closeModal }) => {
    const [appointmentType, setAppointmentType] = useState('consultation')

    return (
        <Modal show={showModal} size='lg' aria-labelledby='contained-modal-title-vcenter' centered>
            <Modal.Header>
                <Modal.Title id='contained-modal-title-vcenter'>Set Appointment</Modal.Title>
                <div
                    className='btn btn-sm btn-icon btn-active-color-primary'
                    onClick={() => closeModal()}
                >
                    <KTSVG path='/media/icons/duotune/arrows/arr061.svg' className='svg-icon-1' />
                </div>
            </Modal.Header>
            <Modal.Body className='py-lg-10 px-lg-10'>
                <Formik
                    initialValues={{ AppointmentType: 'consultation' }}
                    onSubmit={() => closeModal()}
                >
                    {({ values, handleChange, handleSubmit }) => (
                        <form onSubmit={handleSubmit}>
                            <div className='w-100'>
                                <div className='row'>
                                    <div className='col-md-3'>
                                        <div className='w-100'>
                                            <div className='fv-row mb-10'>
                                                <label className='d-flex align-items-center fs-5 fw-bold mb-2'>
                                                    <span className='required'>Date</span>
                                                </label>

                                                <Field
                                                    type='date'
                                                    id='date'
                                                    name='date'
                                                    className='form-control form-control-lg form-control-solid'
                                                    value={'2022-08-01'}
                                                />
                                            </div>
                                        </div>
                                    </div>
                                    <div className='col-md-3'>
                                        <div className='w-100'>
                                            <div className='fv-row mb-10'>
                                                <label className='d-flex align-items-center fs-5 fw-bold mb-2'>
                                                    <span className='required'>Duration</span>
                                                </label>
                                                <Field
                                                    type='text'
                                                    id='duration'
                                                    name='duration'
                                                    placeholder='Duration'
                                                    className='form-control form-control form-control-solid me-1'
                                                />
                                            </div>
                                        </div>
                                    </div>
                                    <div className='col-md-3'>
                                        <div className='w-100'>
                                            <div className='fv-row mb-10'>
                                                <label className='d-flex align-items-center fs-5 fw-bold mb-2'>
                                                    <span className='required'>Start Time</span>
                                                </label>
                                                <Field
                                                    type='time'
                                                    id='startTime'
                                                    name='startTime'
                                                    placeholder='Start Time'
                                                    className='form-control form-control form-control-solid me-1'
                                                />
                                            </div>
                                        </div>
                                    </div>
                                    <div className='col-md-3'>
                                        <div className='w-100'>
                                            <div className='fv-row mb-10'>
                                                <label className='d-flex align-items-center fs-5 fw-bold mb-2'>
                                                    <span>End Time</span>
                                                </label>
                                                <div className='col-md-6 mt-5'>04:07 PM</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div className='w-100'>
                                <div className='row'>
                                    <div className='col-md-6'>
                                        <div className='w-100'>
                                            <div className='fv-row mb-10'>
                                                <label className='d-flex align-items-center fs-5 fw-bold mb-2'>
                                                    <span className='required'>
                                                        Appointment Type
                                                    </span>
                                                </label>
                                                <div className='row'>
                                                    <div className='col-md-6 col-sm-4 py-2'>
                                                        <Field
                                                            type='radio'
                                                            className='btn-check'
                                                            name='AppointmentType'
                                                            id='consultation'
                                                            value='consultation'
                                                            selected={true}
                                                            onClick={() =>
                                                                setAppointmentType('consultation')
                                                            }
                                                        />
                                                        <label
                                                            className='btn btn-outline btn-outline-dashed btn-outline-default w-100 p-4'
                                                            htmlFor='consultation'
                                                        >
                                                            <span className='fw-bolder fs-6'>
                                                                Consultation
                                                            </span>
                                                        </label>
                                                    </div>
                                                    <div className='col-md-6 col-sm-4 py-2'>
                                                        <Field
                                                            type='radio'
                                                            className='btn-check'
                                                            name='AppointmentType'
                                                            id='Therapy'
                                                            value='Therapy'
                                                            onClick={() =>
                                                                setAppointmentType('therapy')
                                                            }
                                                        />
                                                        <label
                                                            className='btn btn-outline btn-outline-dashed btn-outline-default w-100 p-4'
                                                            htmlFor='Therapy'
                                                        >
                                                            <span className='fw-bolder fs-6'>
                                                                Therapy
                                                            </span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className='col-md-6'>
                                        <div className='w-100'>
                                            <div className='fv-row mb-10'>
                                                <label className='d-flex align-items-center fs-5 fw-bold mb-2'>
                                                    <span className='required'>Department</span>
                                                </label>
                                                <div className='row'>
                                                    <div className='col-md-6 col-sm-4 py-2'>
                                                        <Field
                                                            type='radio'
                                                            className='btn-check'
                                                            name='Department'
                                                            id='DayCare'
                                                            checked={true}
                                                        />
                                                        <label
                                                            className='btn btn-outline btn-outline-dashed btn-outline-default w-100 p-4'
                                                            htmlFor='DayCare'
                                                        >
                                                            <span className='fw-bolder fs-6'>
                                                                Day Care
                                                            </span>
                                                        </label>
                                                    </div>
                                                    <div className='col-md-6 col-sm-4 py-2'>
                                                        <Field
                                                            type='radio'
                                                            className='btn-check'
                                                            name='Department'
                                                            id='Yoga'
                                                        />
                                                        <label
                                                            className='btn btn-outline btn-outline-dashed btn-outline-default w-100 p-4'
                                                            htmlFor='Yoga'
                                                        >
                                                            <span className='fw-bolder fs-6'>
                                                                Yoga
                                                            </span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            {appointmentType === 'consultation' && (
                                <div className='w-100'>
                                    <div className='fv-row mb-10'>
                                        <label className='d-flex align-items-center fs-5 fw-bold mb-2'>
                                            <span className='required'>Doctor</span>
                                        </label>

                                        <Field
                                            as='select'
                                            className='form-control form-control-lg form-control-solid'
                                            name='doctor'
                                            onChange={handleChange}
                                            autoFocus={true}
                                        >
                                            <option>Select Doctor</option>
                                            <option>Chetan Khandala</option>
                                            <option>Dainit Test</option>
                                        </Field>
                                    </div>
                                </div>
                            )}

                            {appointmentType === 'therapy' && (
                                <>
                                    <div className='w-100'>
                                        <div className='row'>
                                            <div className='col-md-4'>
                                                <div className='fv-row mb-10'>
                                                    <label className='d-flex align-items-center fs-5 fw-bold mb-2'>
                                                        <span className='required'>Therapy</span>
                                                    </label>

                                                    <Field
                                                        as='select'
                                                        className='form-control form-control-lg form-control-solid'
                                                        name='therapy'
                                                        onChange={handleChange}
                                                        autoFocus={true}
                                                    >
                                                        <option>Therapy 1</option>
                                                        <option>Therapy 2</option>
                                                        <option>Therapy 3</option>
                                                        <option>Therapy 4</option>
                                                        <option>Therapy 5</option>
                                                        <option>Therapy 6</option>
                                                    </Field>
                                                </div>
                                            </div>
                                            <div className='col-md-4'>
                                                <div className='fv-row mb-10'>
                                                    <label className='d-flex align-items-center fs-5 fw-bold mb-2'>
                                                        <span className='required'>Therapist</span>
                                                    </label>

                                                    <Field
                                                        as='select'
                                                        className='form-control form-control-lg form-control-solid'
                                                        name='therapist'
                                                        onChange={handleChange}
                                                        autoFocus={true}
                                                    >
                                                        <option>Therapist 1</option>
                                                        <option>Therapist 2</option>
                                                        <option>Therapist 3</option>
                                                        <option>Therapist 4</option>
                                                        <option>Therapist 5</option>
                                                        <option>Therapist 6</option>
                                                    </Field>
                                                </div>
                                            </div>
                                            <div className='col-md-4'>
                                                <div className='fv-row mb-10'>
                                                    <label className='d-flex align-items-center fs-5 fw-bold mb-2'>
                                                        <span className='required'>Room</span>
                                                    </label>

                                                    <Field
                                                        as='select'
                                                        className='form-control form-control-lg form-control-solid'
                                                        name='Room'
                                                        onChange={handleChange}
                                                        autoFocus={true}
                                                    >
                                                        <option>Room 1</option>
                                                        <option>Room 2</option>
                                                        <option>Room 3</option>
                                                        <option>Room 4</option>
                                                        <option>Room 5</option>
                                                        <option>Room 6</option>
                                                    </Field>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </>
                            )}

                            <div className='w-100'>
                                <div className='fv-row mb-10'>
                                    <label className='d-flex align-items-center fs-5 fw-bold mb-2'>
                                        <span className='required'>Remark</span>
                                    </label>

                                    <Field
                                        as='textarea'
                                        className='form-control form-control-lg form-control-solid'
                                        name='remark'
                                        onChange={handleChange}
                                        placeholder='Remark'
                                    />
                                </div>
                            </div>

                            <div className='w-100'>
                                <button
                                    type='submit'
                                    className='btn btn-lg btn-success me-3 pull-right'
                                >
                                    Save
                                </button>
                            </div>
                        </form>
                    )}
                </Formik>
            </Modal.Body>
        </Modal>
    )
}

export default SetAppointment
