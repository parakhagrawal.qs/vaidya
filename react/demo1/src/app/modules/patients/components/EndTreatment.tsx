import { Field, Formik } from 'formik'
import { FC, useState } from 'react'
import { Modal } from 'react-bootstrap'
import {KTSVG} from "../../../../_metronic/helpers/components/KTSVG";

type Props = {
    showModal: boolean
    closeModal: Function
}

const EndTreatment: FC<Props> = ({ showModal, closeModal }) => {
    const [treatmentType, setTreatmentType] = useState('Ongoing')

    return (
        <Modal show={showModal} size='lg' aria-labelledby='contained-modal-title-vcenter' centered>
            <Modal.Header>
                <Modal.Title id='contained-modal-title-vcenter'>End Treatment</Modal.Title>
                <div
                    className='btn btn-sm btn-icon btn-active-color-primary'
                    onClick={() => closeModal()}
                >
                    <KTSVG path='/media/icons/duotune/arrows/arr061.svg' className='svg-icon-1' />
                </div>
            </Modal.Header>
            <Modal.Body className='py-lg-10 px-lg-10'>
                <Formik
                    initialValues={{
                        TreatmentType: 'Ongoing',
                        ResultType: 'Success',
                        Satisfiaction: 'Satisfied',
                    }}
                    onSubmit={() => closeModal()}
                >
                    {({ values, handleChange, handleSubmit }) => (
                        <form onSubmit={handleSubmit}>
                            <div className='w-100'>
                                <div className='fv-row mb-10'>
                                    <label className='d-flex align-items-center fs-5 fw-bold mb-2'>
                                        <span className='required'>Treatment</span>
                                    </label>

                                    <div className='row'>
                                        <div className='col-md-4 col-sm-4 py-2'>
                                            <Field
                                                type='radio'
                                                className='btn-check'
                                                name='TreatmentType'
                                                id='Ongoing'
                                                value='Ongoing'
                                                selected={true}
                                                onClick={() => setTreatmentType('Ongoing')}
                                            />
                                            <label
                                                className='btn btn-outline btn-outline-dashed btn-outline-default w-100 p-4'
                                                htmlFor='Ongoing'
                                            >
                                                <span className='fw-bolder fs-6'>Ongoing</span>
                                            </label>
                                        </div>
                                        <div className='col-md-4 col-sm-4 py-2'>
                                            <Field
                                                type='radio'
                                                className='btn-check'
                                                name='TreatmentType'
                                                id='Incomplete'
                                                value='Incomplete'
                                                onClick={() => setTreatmentType('Incomplete')}
                                            />
                                            <label
                                                className='btn btn-outline btn-outline-dashed btn-outline-default w-100 p-4'
                                                htmlFor='Incomplete'
                                            >
                                                <span className='fw-bolder fs-6'>Incomplete</span>
                                            </label>
                                        </div>
                                        <div className='col-md-4 col-sm-4 py-2'>
                                            <Field
                                                type='radio'
                                                className='btn-check'
                                                name='TreatmentType'
                                                id='Complete'
                                                value='Complete'
                                                onClick={() => setTreatmentType('Complete')}
                                            />
                                            <label
                                                className='btn btn-outline btn-outline-dashed btn-outline-default w-100 p-4'
                                                htmlFor='Complete'
                                            >
                                                <span className='fw-bolder fs-6'>Complete</span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {treatmentType === 'Incomplete' && (
                                <div className='w-100'>
                                    <div className='fv-row mb-10'>
                                        <label className='d-flex align-items-center fs-5 fw-bold mb-2'>
                                            <span className='required'>Reason for incomplete</span>
                                        </label>

                                        <Field
                                            as='select'
                                            id='reasonForIncomplete'
                                            name='ReasonForIncomplete'
                                            className='form-control form-control-lg form-control-solid'
                                        >
                                            <option>Doctor Mistake</option>
                                            <option>Medicine didn't worked</option>
                                            <option>Money Problem</option>
                                            <option>Diet not followed</option>
                                            <option>Diet and lifestyle not followed</option>
                                            <option>No follow up</option>
                                            <option>Don't know</option>
                                            <option>Other</option>
                                        </Field>
                                    </div>
                                </div>
                            )}
                            <div className='w-100'>
                                <div className='fv-row mb-10'>
                                    <label className='d-flex align-items-center fs-5 fw-bold mb-2'>
                                        <span className='required'>Result Type</span>
                                    </label>

                                    <div className='row'>
                                        <div className='col-md-4 col-sm-4 py-2'>
                                            <Field
                                                type='radio'
                                                className='btn-check'
                                                name='ResultType'
                                                id='Success'
                                                value='Success'
                                            />
                                            <label
                                                className='btn btn-outline btn-outline-dashed btn-outline-default w-100 p-4'
                                                htmlFor='Success'
                                            >
                                                <span className='fw-bolder fs-6'>Success</span>
                                            </label>
                                        </div>
                                        <div className='col-md-4 col-sm-4 py-2'>
                                            <Field
                                                type='radio'
                                                className='btn-check'
                                                name='ResultType'
                                                id='Failure'
                                                value='Failure'
                                                selected={true}
                                            />
                                            <label
                                                className='btn btn-outline btn-outline-dashed btn-outline-default w-100 p-4'
                                                htmlFor='Failure'
                                            >
                                                <span className='fw-bolder fs-6'>Failure</span>
                                            </label>
                                        </div>
                                        <div className='col-md-4 col-sm-4 py-2'>
                                            <Field
                                                type='radio'
                                                className='btn-check'
                                                name='ResultType'
                                                id='NA'
                                                value='NA'
                                            />
                                            <label
                                                className='btn btn-outline btn-outline-dashed btn-outline-default w-100 p-4'
                                                htmlFor='NA'
                                            >
                                                <span className='fw-bolder fs-6'>N/A</span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className='w-100'>
                                <div className='fv-row mb-10'>
                                    <label className='d-flex align-items-center fs-5 fw-bold mb-2'>
                                        <span className='required'>Satisfaction</span>
                                    </label>

                                    <div className='row'>
                                        <div className='col-md-4 col-sm-4 py-2'>
                                            <Field
                                                type='radio'
                                                className='btn-check'
                                                name='Satisfiaction'
                                                id='Satisfied'
                                                value='Satisfied'
                                            />
                                            <label
                                                className='btn btn-outline btn-outline-dashed btn-outline-default w-100 p-4'
                                                htmlFor='Satisfied'
                                            >
                                                <span className='fw-bolder fs-6'>Satisfied</span>
                                            </label>
                                        </div>
                                        <div className='col-md-4 col-sm-4 py-2'>
                                            <Field
                                                type='radio'
                                                className='btn-check'
                                                name='Satisfiaction'
                                                id='NotSatisfied'
                                                value='NotSatisfied'
                                                selected={true}
                                            />
                                            <label
                                                className='btn btn-outline btn-outline-dashed btn-outline-default w-100 p-4'
                                                htmlFor='NotSatisfied'
                                            >
                                                <span className='fw-bolder fs-6'>
                                                    Not Satisfied
                                                </span>
                                            </label>
                                        </div>
                                        <div className='col-md-4 col-sm-4 py-2'>
                                            <Field
                                                type='radio'
                                                className='btn-check'
                                                name='Satisfiaction'
                                                id='NA'
                                                value='NA'
                                            />
                                            <label
                                                className='btn btn-outline btn-outline-dashed btn-outline-default w-100 p-4'
                                                htmlFor='NA'
                                            >
                                                <span className='fw-bolder fs-6'>N/A</span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className='w-100'>
                                <div className='fv-row mb-10'>
                                    <label className='d-flex align-items-center fs-5 fw-bold mb-2'>
                                        <span className='required'>Result (%)</span>
                                    </label>
                                    <Field
                                        type='text'
                                        id='result'
                                        name='result'
                                        placeholder='Result (%)'
                                        className='form-control form-control form-control-solid me-1'
                                    />
                                </div>
                            </div>
                            <div className='w-100'>
                                <div className='fv-row mb-10'>
                                    <label className='d-flex align-items-center fs-5 fw-bold mb-2'>
                                        <span className='required'>Remarks</span>
                                    </label>
                                    <Field
                                        as='textarea'
                                        id='Remark'
                                        name='Remark'
                                        placeholder='Remarks'
                                        className='form-control form-control form-control-solid me-1'
                                    />
                                </div>
                            </div>
                            <div className='w-100'>
                                <button
                                    type='submit'
                                    className='btn btn-lg btn-success me-3 pull-right'
                                >
                                    Save
                                </button>
                            </div>
                        </form>
                    )}
                </Formik>
            </Modal.Body>
        </Modal>
    )
}

export default EndTreatment
