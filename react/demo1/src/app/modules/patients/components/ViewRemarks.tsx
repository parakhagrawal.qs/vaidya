/* eslint-disable jsx-a11y/anchor-is-valid */
import { FC } from 'react'
import { Modal } from 'react-bootstrap'
import {KTSVG} from "../../../../_metronic/helpers/components/KTSVG"

type Props = {
    showModal: boolean
    closeModal: Function
}

const ViewRemarks: FC<Props> = ({ showModal, closeModal }) => {
    const remarks = [
        {
            Date: '01-Aug-2022',
            Time: '04:50 PM',
            Remark: 'Test Remark 1',
            CallBy: 'Mobile',
        },
        {
            Date: '01-Aug-2022',
            Time: '05:50 PM',
            Remark: 'Test Remark 2',
            CallBy: 'Mobile',
        },
    ]

    return (
        <Modal show={showModal} size='lg' aria-labelledby='contained-modal-title-vcenter' centered>
            <Modal.Header>
                <Modal.Title id='contained-modal-title-vcenter'>Remark Information</Modal.Title>
                <div
                    className='btn btn-sm btn-icon btn-active-color-primary'
                    onClick={() => closeModal()}
                >
                    <KTSVG path='/media/icons/duotune/arrows/arr061.svg' className='svg-icon-1' />
                </div>
            </Modal.Header>
            <Modal.Body className='py-lg-10 px-lg-6'>
                <div className='table-responsive'>
                    <table className='table table-row-dashed table-row-gray-300 align-middle gs-0 gy-4'>
                        <thead>
                            <tr className='fw-bolder text-muted'>
                                <th className='min-w-150px fw-bolder'>Date</th>
                                <th className='min-w-150px fw-bolder'>Time</th>
                                <th className='min-w-150px fw-bolder'>Remark</th>
                                <th className='min-w-150px fw-bolder'>Call By</th>
                                <th className='min-w-150px fw-bolder'>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            {remarks.length > 0 &&
                                remarks.map((remark) => {
                                    return (
                                        <tr>
                                            <td>{remark.Date}</td>
                                            <td>{remark.Time}</td>
                                            <td>{remark.Remark}</td>
                                            <td>{remark.CallBy}</td>
                                            <td>
                                                <div className='d-flex justify-content-end flex-shrink-0'>
                                                    <a
                                                        href='#'
                                                        className='btn btn-icon btn-bg-light btn-active-color-danger btn-sm me-1'
                                                        title='Delete'
                                                    >
                                                        <KTSVG
                                                            path='/media/icons/duotune/general/gen027.svg'
                                                            className='svg-icon-3'
                                                        />
                                                    </a>
                                                </div>
                                            </td>
                                        </tr>
                                    )
                                })}
                            {remarks.length === 0 && (
                                <tr>
                                    <td colSpan={5}>
                                        <div className='align-items-center text-center'>
                                            No records found.
                                        </div>
                                    </td>
                                </tr>
                            )}
                        </tbody>
                    </table>
                </div>
            </Modal.Body>
        </Modal>
    )
}

export default ViewRemarks
