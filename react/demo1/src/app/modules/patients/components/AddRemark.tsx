import { Field, Formik } from 'formik'
import { FC } from 'react'
import { Modal } from 'react-bootstrap'
import {KTSVG} from "../../../../_metronic/helpers/components/KTSVG";

type Props = {
    showModal: boolean
    closeModal: Function
}

const AddRemark: FC<Props> = ({ showModal, closeModal }) => {
    return (
        <Modal show={showModal} size='lg' aria-labelledby='contained-modal-title-vcenter' centered>
            <Modal.Header>
                <Modal.Title id='contained-modal-title-vcenter'>Add Remark</Modal.Title>
                <div
                    className='btn btn-sm btn-icon btn-active-color-primary'
                    onClick={() => closeModal()}
                >
                    <KTSVG path='/media/icons/duotune/arrows/arr061.svg' className='svg-icon-1' />
                </div>
            </Modal.Header>
            <Modal.Body className='py-lg-10 px-lg-10'>
                <Formik initialValues={{}} onSubmit={() => closeModal()}>
                    {({ values, handleChange, handleSubmit }) => (
                        <form onSubmit={handleSubmit}>
                            <div className='w-100'>
                                <div className='row'>
                                    <div className='col-md-6'>
                                        <div className='fv-row mb-10'>
                                            <label className='d-flex align-items-center fs-5 fw-bold mb-2'>
                                                <span className='required'>Call By</span>
                                            </label>

                                            <Field
                                                type='text'
                                                id='callBy'
                                                name='CallBy'
                                                className='form-control form-control-lg form-control-solid'
                                                autoFocus={true}
                                                placeholder='Call By'
                                            />
                                        </div>
                                    </div>
                                    <div className='col-md-6'>
                                        <div className='w-100'>
                                            <div className='fv-row mb-10'>
                                                <label className='d-flex align-items-center fs-5 fw-bold mb-2'>
                                                    <span className='required'>Next Follow Up</span>
                                                </label>

                                                <Field
                                                    type='date'
                                                    id='date'
                                                    name='date'
                                                    className='form-control form-control-lg form-control-solid'
                                                    value={'2022-08-01'}
                                                />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className='w-100'>
                                <div className='fv-row mb-10'>
                                    <label className='d-flex align-items-center fs-5 fw-bold mb-2'>
                                        <span className='required'>Remarks</span>
                                    </label>

                                    <Field
                                        as='textarea'
                                        id='Remarks'
                                        name='CalRemarks'
                                        className='form-control form-control-lg form-control-solid'
                                        autoFocus={true}
                                        placeholder='Remarks'
                                    />
                                </div>
                            </div>
                            <div className='w-100'>
                                <button
                                    type='submit'
                                    className='btn btn-lg btn-success me-3 pull-right'
                                >
                                    Save
                                </button>
                            </div>
                        </form>
                    )}
                </Formik>
            </Modal.Body>
        </Modal>
    )
}

export default AddRemark
