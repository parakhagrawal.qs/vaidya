/* eslint-disable jsx-a11y/anchor-is-valid */
import { Field, Formik } from 'formik'
import { FC, useState } from 'react'
import { NavLink } from 'react-router-dom'
import {KTSVG  } from "../../../_metronic/helpers/components/KTSVG";
import SetAppointment from './components/SetAppointment'
import AddRemark from './components/AddRemark'
import EndTreatment from './components/EndTreatment'
// import SendSms from './SendSms'
import ViewRemarks from './components/ViewRemarks'

const PatientsHeader: FC = () => {
    const patients = [
        {
            caseId: 'OP/22-23/223',
            SearchName: 'Mr. Kulkarni',
            Diagnosis: '',
            Mobile: '',
            locality: 'Mumbai',
        },
        {
            caseId: 'OP/22-23/224',
            SearchName: 'Ms. Dhara',
            Diagnosis: 'Aam Sandhigata vata',
            Mobile: '+91 9426146310',
            locality: 'Ahmedabad',
        },
    ]

    const [showSms, setShowSms] = useState(false)
    const [showSetAppointment, setShowSetAppointment] = useState(false)
    const [showAddRemark, setShowAddRemark] = useState(false)
    const [showViewRemarks, setShowViewRemarks] = useState(false)
    const [showEndTreatment, setShowEndTreatment] = useState(false)

    return (
        <>
            <div className='card'>
                {/* begin::Header */}
                <div className='card-header border-0 pt-5'>
                    <h3 className='card-title align-items-start flex-column'>
                        <span className='card-label fw-bolder fs-3 mb-1'>Patients</span>
                    </h3>
                </div>
                {/* end::Header */}
                {/* begin::Body */}
                <div className='card-body py-3'>
                    {/* begin::Table container */}
                    <Formik initialValues={{}} onSubmit={() => {}}>
                        {({ values, handleChange, handleSubmit }) => (
                            <form onSubmit={handleSubmit}>
                                <div className='w-100'>
                                    <div className='row fv-row mb-10'>
                                        <div className='col-md-6'>
                                            <label className='d-flex align-items-center fs-5 fw-bold mb-2'>
                                                <span>Patient Type</span>
                                            </label>
                                            <Field
                                                as='select'
                                                id='ddlPatientType'
                                                name='PatientType'
                                                className='form-control form-control-lg form-control-solid'
                                            >
                                                <option value='Active Patients'>
                                                    Active Patients
                                                </option>
                                                <option value='Wellness Patients'>
                                                    Wellness Patients
                                                </option>
                                                <option value='Complete Patients'>
                                                    Complete Patients
                                                </option>
                                                <option value='Incomplete Patients'>
                                                    Incomplete Patients
                                                </option>
                                                <option value='Favourite Patients'>
                                                    Favourite Patients
                                                </option>
                                                <option value='All'>All</option>
                                            </Field>
                                        </div>
                                        <div className='col-md-6'>
                                            <label className='d-flex align-items-center fs-5 fw-bold mb-2'>
                                                <span>Date</span>
                                            </label>
                                            <Field
                                                type='date'
                                                id='filterDate'
                                                name='filterDate'
                                                className='form-control form-control-lg form-control-solid'
                                                value={'2022-08-01'}
                                            />
                                        </div>
                                    </div>
                                </div>
                            </form>
                        )}
                    </Formik>
                    <div className='table-responsive'>
                        <table className='table table-row-dashed table-row-gray-300 align-middle gs-0 gy-4'>
                            <thead>
                                <tr className='fw-bolder text-muted'>
                                    <th className='w-25px'>
                                        <div className='form-check form-check-sm form-check-custom form-check-solid'>
                                            <input
                                                className='form-check-input'
                                                type='checkbox'
                                                value='0'
                                                data-kt-check='true'
                                                data-kt-check-target='.widget-9-check'
                                            />
                                        </div>
                                    </th>
                                    <th className='min-w-150px fw-bolder'>Case ID</th>
                                    <th className='min-w-150px fw-bolder'>Search Name</th>
                                    <th className='min-w-150px fw-bolder'>
                                        Diagnosis / Modern System
                                    </th>
                                    <th className='min-w-150px fw-bolder'>Mobile</th>
                                    <th className='min-w-150px fw-bolder'>Area / City</th>
                                    <th className='min-w-150px fw-bolder'>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <>
                                    {patients.map((patient) => {
                                        return (
                                            <>
                                                <tr>
                                                    <td>
                                                        <div className='form-check form-check-sm form-check-custom form-check-solid'>
                                                            <input
                                                                className='form-check-input widget-9-check'
                                                                type='checkbox'
                                                            />
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <NavLink to='/patients/detail'>
                                                            {patient.caseId}
                                                        </NavLink>
                                                    </td>
                                                    <td>{patient.SearchName}</td>
                                                    <td>{patient.Diagnosis}</td>
                                                    <td>
                                                        <span onClick={() => setShowSms(true)}>
                                                            {patient.Mobile}
                                                        </span>
                                                    </td>
                                                    <td>{patient.locality}</td>
                                                    <td>
                                                        <div className='d-flex justify-content-end flex-shrink-0'>
                                                            <a
                                                                href='#'
                                                                className='btn btn-icon btn-bg-light btn-active-color-danger btn-sm me-1'
                                                                title='Set Appointment'
                                                                onClick={() =>
                                                                    setShowSetAppointment(true)
                                                                }
                                                            >
                                                                <i className='fas fa-clock px-2'></i>
                                                            </a>
                                                            <a
                                                                href='#'
                                                                className='btn btn-icon btn-bg-light btn-active-color-danger btn-sm me-1'
                                                                title='Add to waiting list'
                                                            >
                                                                <i className='fas fa-arrow-right px-2' />
                                                            </a>
                                                            <a
                                                                href='#'
                                                                className='btn btn-icon btn-bg-light btn-active-color-danger btn-sm me-1'
                                                                title='Add Remark'
                                                                onClick={() =>
                                                                    setShowAddRemark(true)
                                                                }
                                                            >
                                                                <i className='fas fa-plus px-2' />
                                                            </a>
                                                            <a
                                                                href='#'
                                                                className='btn btn-icon btn-bg-light btn-active-color-danger btn-sm me-1'
                                                                title='View Remark'
                                                                onClick={() =>
                                                                    setShowViewRemarks(true)
                                                                }
                                                            >
                                                                <i className="fas fa-info-circle"></i>
                                                                {/* <i className='fa-solid fa-circle-info px-2' /> */}
                                                            </a>
                                                            <a
                                                                href='#'
                                                                className='btn btn-icon btn-bg-light btn-active-color-primary btn-sm me-1'
                                                                title='Edit Status'
                                                                onClick={() =>
                                                                    setShowEndTreatment(true)
                                                                }
                                                            >
                                                                <KTSVG
                                                                    path='/media/icons/duotune/art/art005.svg'
                                                                    className='svg-icon-3'
                                                                />
                                                            </a>
                                                            <a
                                                                href='#'
                                                                className='btn btn-icon btn-bg-light btn-active-color-danger btn-sm me-1'
                                                                title='Delete'
                                                            >
                                                                <KTSVG
                                                                    path='/media/icons/duotune/general/gen027.svg'
                                                                    className='svg-icon-3'
                                                                />
                                                            </a>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </>
                                        )
                                    })}
                                </>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            {/* <SendSms showModal={showSms} closeModal={() => setShowSms(false)} /> */}
            <SetAppointment
                showModal={showSetAppointment}
                closeModal={() => setShowSetAppointment(false)}
            />
            <AddRemark showModal={showAddRemark} closeModal={() => setShowAddRemark(false)} />
            <ViewRemarks showModal={showViewRemarks} closeModal={() => setShowViewRemarks(false)} />
            <EndTreatment
                showModal={showEndTreatment}
                closeModal={() => setShowEndTreatment(false)}
            />
        </>
    )
}

export default PatientsHeader
