import React from 'react'
import {Navigate, Route, Routes, Outlet} from 'react-router-dom'
import {PageLink, PageTitle} from '../../../_metronic/layout/core'
// import {Overview} from './components/Overview'
// import {Settings} from './components/settings/Settings'

import PatientsHeader from './PatientsList'

const patientBreadCrumbs: Array<PageLink> = [
  {
    title: 'Patient List',
    path: '/crafted/patient/list',
    isSeparator: false,
    isActive: false,
  },
  {
    title: '',
    path: '',
    isSeparator: true,
    isActive: false,
  },
]

const PatientsPage: React.FC = () => {
  return (
    <Routes>
      <Route
        element={
          <>
            <PatientsHeader />
            <Outlet />
          </>
        }
      >
        <Route
          path='list'
          element={
            <>
              <PageTitle breadcrumbs={patientBreadCrumbs}>Patients List</PageTitle>
              {/* <Overview /> */}
            </>
          }
        />
        {/* <Route
          path='settings'
          element={
            <>
              <PageTitle breadcrumbs={patientBreadCrumbs}>Settings</PageTitle>
              <Settings />
            </>
          }
        /> */}
        {/* <Route index element={<Navigate to='/crafted/patient/list' />} /> */}
      </Route>
    </Routes>
  )
}

export default PatientsPage

